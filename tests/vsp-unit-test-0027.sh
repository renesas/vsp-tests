#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2017-2022 Renesas Electronics Corporation

#
# Test RPF crop using multiplanar YUV formats, test buffer offset calculation
# with subsampling. Use a RPF -> WPF pipeline, passing a selection of cropping
# windows.
#

. ./vsp-lib.sh

features="rpf.0 wpf.0"
crops="(0,0)/512x384 (32,32)/512x384 (32,64)/512x384 (64,32)/512x384"
formats="NV12M NV16M YUV420M YUV422M YUV444M"

test_rpf_cropping() {
	local format=$1
	local crop=$2

	test_start "RPF crop from $crop in $format"

	pipe_configure rpf-wpf 0 0
	format_configure rpf-wpf 0 0 $format 1024x768 YUV444M --rpfcrop=$crop

	vsp_runner rpf.0 &
	vsp_runner wpf.0

	local result=$(compare_frames crop=${crop})

	test_complete $result
}

test_main() {
	local crop
	local format

	for format in $formats ; do
		for crop in $crops ; do
			test_rpf_cropping $format $crop
		done
	done
}

test_init $0 "$features"
test_run
